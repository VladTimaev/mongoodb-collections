const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/fruitsDB');

const fruitSchema = new mongoose.Schema ({
  name: String,
  rating: {
    type:Number,
    min: 1,
    max: 10
  },
  review: String
});

const Fruit = mongoose.model("Fruit", fruitSchema);

const fruit = new Fruit ({
  rating: 10,
  review: "Have a good day!"
});

const apple = new Fruit ({
  name:"PineApple",
  rating:10,
  review:"PineApple Paradise"
});

const greenApple = new Fruit ({
  name:"GreenApple",
  rating: 5,
  review:"GreenApple is a very beautiful"
});
// fruit.save();

Fruit.deleteOne({_id:"625a747091f0271f55f3b150"}, function(err){
  if(err){
    console.log("Error");
  } else {
    console.log("successfully update the documentation!");
  }
});


// Fruit.insertMany([kiwi,orange,banana], function(err){
//   if(err){
//     console.log("It's not successfully");
//   } else{
//     console.log("All fruits saved successfully to Database!");
//   }
// });
Fruit.find(function(err, fruits){
  if(err){
   console.log(err);
 } else{
   fruits.forEach(function(fruit){
     console.log(fruit.name);
   });
 }
});
const pineApple = new Fruit({
  name: "pineApple",
  score: 9,
  review: "Great fruit."
});
 pineApple.save();

const personSchema = new mongoose.Schema ({
  name: String,
  age: Number,
  favoriteFruit: fruitSchema
});

const People = mongoose.model('People', personSchema);
const people = new People ({
  name: "Vica",
  surname:"Timaeva",
  age: 37,
  favoriteFruit: pineApple
});
const s = new People ({
  name: "Vlad",
  surname:"Timaeva",
  age: 37,
  favoriteFruit: apple
});
s.save();

Fruit.deleteMany({ name: "RedApple"}, function(err){
  if(err){
    console.log("Errors");
  } else{
    console.log("Successfully deleted all the fruits");
  };
}); // returns {deletedCount: x} where x is the numbe












// const assert = require("assert");
// // Replace the uri string with your MongoDB deployment's connection string.
// // const url = "mongodb://localhost:27017";
//
// const dbName = "fruitsDB";
//
// const client = new MongoClient(url);
//
// client.connect(function(err){
//   assert.equal(null,err);
//   console.log("Connected successfully to server");
//
//   const db = client.db(dbName);
//   findDocuments(db, function(){
//     client.close();
//   });
// });


const insertDocuments = function(db, callback){
  // Get the dociments collection
  const collection = db.collection("fruits");
  // Insert some insertDocuments
  collection.insertMany([
    {
     name:"Apple",
     score: 8,
     review:"Great fruit"
    },
    {
      name:"Orange",
      score: 6,
      review:"Kinda sour"
    },
    {
      name:"Banana",
      score: 9,
      review:"Great stuff"
    }
  ], function(err,result){

    console.log("Inserted 3 documents into the collections");
    callback(result);
  });
};

const findDocuments = function(db, callback){
  const collection = db.collection("fruits");
  // Find some findDocuments
  collection.find({}).toArray(function(err, fruits){
    assert.equal(err, null);
    console.log("Found the following records");
    console.log(fruits)
    callback(fruits);
  });
}
